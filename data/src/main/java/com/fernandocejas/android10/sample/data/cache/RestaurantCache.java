/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.cache;

import com.fernandocejas.android10.sample.data.entity.RestaurantEntity;

import rx.Observable;

/**
 * An interface representing a restaurant Cache.
 */
public interface RestaurantCache {
    /**
     * Gets an {@link rx.Observable} which will emit a {@link RestaurantEntity}.
     *
     * @param restaurantID The restaurant id to retrieve data.
     */
    Observable<RestaurantEntity> get(final int restaurantID);

    /**
     * Puts and element into the cache.
     *
     * @param restaurantEntity Element to insert in the cache.
     */
    void put(RestaurantEntity restaurantEntity);

    /**
     * Checks if an element (Restaurant) exists in the cache.
     *
     * @param restaurantID The id used to look for inside the cache.
     * @return true if the element is cached, otherwise false.
     */
    boolean isCached(final int restaurantID);

    /**
     * Checks if the cache is expired.
     *
     * @return true, the cache is expired, otherwise false.
     */
    boolean isExpired();

    /**
     * Evict all elements of the cache.
     */
    void evictAll();
}

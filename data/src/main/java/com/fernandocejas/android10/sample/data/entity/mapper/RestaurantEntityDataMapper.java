/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.entity.mapper;

import com.fernandocejas.android10.sample.data.entity.RestaurantEntity;
import com.fernandocejas.android10.sample.domain.Restaurant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform {@link RestaurantEntity} (in the data layer) to {@link Restaurant} in the
 * domain layer.
 */
@Singleton
public class RestaurantEntityDataMapper {

    @Inject
    public RestaurantEntityDataMapper() {
    }

    /**
     * Transform a {@link RestaurantEntity} into an {@link Restaurant}.
     *
     * @param restaurantEntity Object to be transformed.
     * @return {@link Restaurant} if valid {@link RestaurantEntity} otherwise null.
     */
    public Restaurant transform(RestaurantEntity restaurantEntity) {
        Restaurant restaurant = null;
        if (restaurantEntity != null) {
            restaurant = new Restaurant(restaurantEntity.getRestaurantId());
            restaurant.setImageURL(restaurantEntity.getImageURL());
            restaurant.setAddress(restaurantEntity.getAddress());
            restaurant.setName(restaurantEntity.getName());
        }

        return restaurant;
    }

    /**
     * Transform a List of {@link RestaurantEntity} into a Collection of {@link Restaurant}.
     *
     * @param restaurantEntityCollection Object Collection to be transformed.
     * @return {@link Restaurant} if valid {@link RestaurantEntity} otherwise null.
     */
    public List<Restaurant> transform(Collection<RestaurantEntity> restaurantEntityCollection) {
        List<Restaurant> restaurantList = new ArrayList<>(20);
        Restaurant restaurant;
        for (RestaurantEntity restaurantEntity : restaurantEntityCollection) {
            restaurant = transform(restaurantEntity);
            if (restaurant != null) {
                restaurantList.add(restaurant);
            }
        }

        return restaurantList;
    }
}

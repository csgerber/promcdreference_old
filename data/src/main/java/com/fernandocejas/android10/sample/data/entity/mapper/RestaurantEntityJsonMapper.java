/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.entity.mapper;

import com.fernandocejas.android10.sample.data.entity.RestaurantEntity;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

/**
 * Class used to transform from Strings representing json to valid objects.
 */
public class RestaurantEntityJsonMapper {

    private final Gson gson;

    @Inject
    public RestaurantEntityJsonMapper() {
        this.gson = new Gson();
    }

    /**
     * Transform from valid json string to {@link RestaurantEntity}.
     *
     * @param restaurantJsonResponse A json representing a restaurant profile.
     * @return {@link RestaurantEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public RestaurantEntity transformRestaurantEntity(String restaurantJsonResponse) throws JsonSyntaxException {
        try {
            Type restaurantEntityType = new TypeToken<RestaurantEntity>() {
            }.getType();
            RestaurantEntity restaurantEntity = this.gson.fromJson(restaurantJsonResponse, restaurantEntityType);

            return restaurantEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link RestaurantEntity}.
     *
     * @param restaurantListJsonResponse A json representing a collection of restaurants.
     * @return List of {@link RestaurantEntity}.
     * @throws com.google.gson.JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<RestaurantEntity> transformRestaurantEntityCollection(String restaurantListJsonResponse)
            throws JsonSyntaxException {

        List<RestaurantEntity> restaurantEntityCollection;
        try {
            Type listOfRestaurantEntityType = new TypeToken<List<RestaurantEntity>>() {
            }.getType();
            restaurantEntityCollection = this.gson.fromJson(restaurantListJsonResponse, listOfRestaurantEntityType);

            return restaurantEntityCollection;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }
}

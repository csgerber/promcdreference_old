/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.repository;

import com.fernandocejas.android10.sample.data.entity.mapper.RestaurantEntityDataMapper;
import com.fernandocejas.android10.sample.data.repository.datasource.RestaurantDataStore;
import com.fernandocejas.android10.sample.data.repository.datasource.RestaurantDataStoreFactory;
import com.fernandocejas.android10.sample.domain.Restaurant;
import com.fernandocejas.android10.sample.domain.repository.RestaurantRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * {@link RestaurantRepository} for retrieving restaurant data.
 */
@Singleton
public class RestaurantDataRepository implements RestaurantRepository {

    private final RestaurantDataStoreFactory restaurantDataStoreFactory;
    private final RestaurantEntityDataMapper restaurantEntityDataMapper;

    /**
     * Constructs a {@link RestaurantRepository}.
     *
     * @param dataStoreFactory A factory to construct different data source implementations.
     * @param restaurantEntityDataMapper {@link RestaurantEntityDataMapper}.
     */
    @Inject
    public RestaurantDataRepository(RestaurantDataStoreFactory dataStoreFactory,
                                    RestaurantEntityDataMapper restaurantEntityDataMapper) {
        this.restaurantDataStoreFactory = dataStoreFactory;
        this.restaurantEntityDataMapper = restaurantEntityDataMapper;
    }

    @Override
    public Observable<List<Restaurant>> restaurants() {
        // TODO: redo this when switching back to cloud (if we do)
//    final RestaurantDataStore restaurantDataStore = this.restaurantDataStoreFactory.createCloudDataStore();
        final RestaurantDataStore restaurantDataStore = this.restaurantDataStoreFactory.createDiskDataStore();
        return restaurantDataStore.restaurantEntityList().map(this.restaurantEntityDataMapper::transform);
    }

    @Override
    public Observable<Restaurant> restaurant(int restaurantID) {
        final RestaurantDataStore restaurantDataStore = this.restaurantDataStoreFactory.create(restaurantID);
        return restaurantDataStore.restaurantEntityDetails(restaurantID).map(this.restaurantEntityDataMapper::transform);
    }
}

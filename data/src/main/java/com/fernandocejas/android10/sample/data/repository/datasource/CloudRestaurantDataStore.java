/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.repository.datasource;

import com.fernandocejas.android10.sample.data.cache.RestaurantCache;
import com.fernandocejas.android10.sample.data.entity.RestaurantEntity;
import com.fernandocejas.android10.sample.data.net.RestApi;

import java.util.List;

import rx.Observable;
import rx.functions.Action1;

// TODO: cloud data store may be beyond the scope of the app, or one of the last things implemented

/**
 * {@link RestaurantDataStore} implementation based on connections to the api (Cloud).
 */
class CloudRestaurantDataStore implements RestaurantDataStore {

    private final RestApi restApi;
    private final RestaurantCache restaurantCache;

    private final Action1<RestaurantEntity> saveToCacheAction = restaurantEntity -> {
        if (restaurantEntity != null) {
            CloudRestaurantDataStore.this.restaurantCache.put(restaurantEntity);
        }
    };

    /**
     * Construct a {@link RestaurantDataStore} based on connections to the api (Cloud).
     *
     * @param restApi The {@link RestApi} implementation to use.
     * @param restaurantCache A {@link RestaurantCache} to cache data retrieved from the api.
     */
    CloudRestaurantDataStore(RestApi restApi, RestaurantCache restaurantCache) {
        this.restApi = restApi;
        this.restaurantCache = restaurantCache;
    }

    @Override
    public Observable<List<RestaurantEntity>> restaurantEntityList() {
        return this.restApi.restaurantEntityList();
    }

    @Override
    public Observable<RestaurantEntity> restaurantEntityDetails(final int restaurantID) {
        return this.restApi.restaurantEntityList(restaurantID).doOnNext(saveToCacheAction);
    }
}

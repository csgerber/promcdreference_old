/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.repository.datasource;

import com.fernandocejas.android10.sample.data.cache.RestaurantCache;
import com.fernandocejas.android10.sample.data.entity.RestaurantEntity;
import com.fernandocejas.android10.sample.data.exception.RestaurantNotFoundException;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;


/**
 * {@link RestaurantDataStore} implementation based on file system data store.
 */
class DiskRestaurantDataStore implements RestaurantDataStore {

    private final RestaurantCache restaurantCache;

    /**
     * Construct a {@link RestaurantDataStore} based file system data store.
     *
     * @param restaurantCache A {@link RestaurantCache} to cache data retrieved from the api.
     */
    DiskRestaurantDataStore(RestaurantCache restaurantCache) {
        this.restaurantCache = restaurantCache;
    }

    // TODO: don't forget to edit this out or delete the diskrestaurantdatastore completely in final
    // note: something like this can easily be ported to test code later on
    @Override
    public Observable<List<RestaurantEntity>> restaurantEntityList() {


        this.restaurantCache.evictAll();

        List<RestaurantEntity> mctmplist = new ArrayList<>();
        int nC = 0;
        RestaurantEntity resto;

        resto = new RestaurantEntity();
        resto.setRestaurantID(++nC);
        resto.setImageURL("https://s3-media4.fl.yelpcdn.com/bphoto/qVfzTJYNjdGrMQRpWHJYJQ/o.jpg");
        resto.setAddress("1951 N Western Ave, Chicago, IL 60647");
        resto.setName("Near North");
        this.restaurantCache.put(resto);
        mctmplist.add(resto);

        resto = new RestaurantEntity();
        resto.setRestaurantID(++nC);
        resto.setImageURL("https://s3-media4.fl.yelpcdn.com/bphoto/UZVwLvOdyI2I3wEPob51Ag/o.jpg");
        resto.setAddress("600 N Clark St, Chicago, IL 60610");
        resto.setName("Old Town");
        this.restaurantCache.put(resto);
        mctmplist.add(resto);

        resto = new RestaurantEntity();
        resto.setRestaurantID(++nC);
        resto.setImageURL("https://s3-media4.fl.yelpcdn.com/bphoto/faxg3V7gL82uRLAauw9fjg/o.jpg");
        resto.setAddress("1951 N Western Ave, Chicago, IL 60647");
        resto.setName("Lake View");
        this.restaurantCache.put(resto);
        mctmplist.add(resto);

        resto = new RestaurantEntity();
        resto.setRestaurantID(++nC);
        resto.setImageURL("https://s3-media1.fl.yelpcdn.com/bphoto/ChpoEknkgHSN0Lu14ix7yg/o.jpg");
        resto.setAddress("1951 N Western Ave, Chicago, IL 60647");
        resto.setName("Lincoln Park");
        this.restaurantCache.put(resto);
        mctmplist.add(resto);

        resto = new RestaurantEntity();
        resto.setRestaurantID(++nC);
        resto.setImageURL("https://s3-media4.fl.yelpcdn.com/bphoto/C5-8jPooSLR07QfWyWSg8w/o.jpg");
        resto.setAddress("1951 N Western Ave, Chicago, IL 60647");
        resto.setName("Hyde Park");
        this.restaurantCache.put(resto);
        mctmplist.add(resto);


        resto = new RestaurantEntity();
        resto.setRestaurantID(++nC);
        resto.setImageURL("https://s3-media2.fl.yelpcdn.com/bphoto/RLMTSWFVRYfOAmBNvwEPVw/o.jpg");
        resto.setAddress("5454 N Western Ave, Evanston, IL 60647");
        resto.setName("Evanston");
        this.restaurantCache.put(resto);
        mctmplist.add(resto);

        resto = new RestaurantEntity();
        resto.setRestaurantID(++nC);
        resto.setImageURL("https://s3-media3.fl.yelpcdn.com/bphoto/RTFKUqIpXGQo8c8EImKosw/o.jpg");
        resto.setAddress("1951 N Western Ave, Chicago, IL 60647");
        resto.setName("Wilmette");
        this.restaurantCache.put(resto);
        mctmplist.add(resto);

        resto = new RestaurantEntity();
        resto.setRestaurantID(++nC);
        resto.setImageURL("https://s3-media4.fl.yelpcdn.com/bphoto/x4au3hc4qg3f_gKbnMiaFA/o.jpg");
        resto.setAddress("1951 N Western Ave, Chicago, IL 60647");
        resto.setName("Highland Park");
        this.restaurantCache.put(resto);
        mctmplist.add(resto);

//        ;
        return Observable.create(subscriber -> {
            if (mctmplist != null) {
                subscriber.onNext(mctmplist);
                subscriber.onCompleted();
            } else {
                subscriber.onError(new RestaurantNotFoundException());
            }
        });

    }

    @Override
    public Observable<RestaurantEntity> restaurantEntityDetails(final int restaurantID) {
        return this.restaurantCache.get(restaurantID);
    }
}

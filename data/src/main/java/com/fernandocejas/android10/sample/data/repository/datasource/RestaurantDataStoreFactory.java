/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.repository.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

import com.fernandocejas.android10.sample.data.cache.RestaurantCache;
import com.fernandocejas.android10.sample.data.entity.mapper.RestaurantEntityJsonMapper;
import com.fernandocejas.android10.sample.data.net.RestApi;
import com.fernandocejas.android10.sample.data.net.RestApiImpl;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Factory that creates different implementations of {@link RestaurantDataStore}.
 */
@Singleton
public class RestaurantDataStoreFactory {

    private final Context context;
    private final RestaurantCache restaurantCache;

    @Inject
    public RestaurantDataStoreFactory(@NonNull Context context, @NonNull RestaurantCache restaurantCache) {
        this.context = context.getApplicationContext();
        this.restaurantCache = restaurantCache;
    }

    // TODO: Rework this when writing the final project, including the RestApi, mapper, etc

    /**
     * Create {@link RestaurantDataStore} from a restaurant id.
     */
    public RestaurantDataStore create(int restaurantId) {
        RestaurantDataStore restaurantDataStore;

        if (!this.restaurantCache.isExpired() && this.restaurantCache.isCached(restaurantId)) {
            restaurantDataStore = new DiskRestaurantDataStore(this.restaurantCache);
        } else {
            restaurantDataStore = createDiskDataStore();
        }

        return restaurantDataStore;

    }

    /**
     * Create {@link RestaurantDataStore} to retrieve data from dummy disk data.
     */
    public RestaurantDataStore createCloudDataStore() {

        RestaurantEntityJsonMapper restaurantEntityJsonMapper = new RestaurantEntityJsonMapper();
        RestApi restApi = new RestApiImpl(this.context, restaurantEntityJsonMapper);

        return new CloudRestaurantDataStore(restApi, this.restaurantCache);
    }

    public RestaurantDataStore createDiskDataStore() {
        return new DiskRestaurantDataStore(this.restaurantCache);
    }
}

/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.entity.mapper;

import com.fernandocejas.android10.sample.data.ApplicationTestCase;
import com.fernandocejas.android10.sample.data.entity.RestaurantEntity;
import com.fernandocejas.android10.sample.domain.Restaurant;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

public class UserEntityDataMapperTest extends ApplicationTestCase {

    private static final int FAKE_USER_ID = 123;
    private static final String FAKE_FULLNAME = "Tony Stark";

    private RestaurantEntityDataMapper userEntityDataMapper;

    @Before
    public void setUp() throws Exception {
        userEntityDataMapper = new RestaurantEntityDataMapper();
    }

    @Test
    public void testTransformUserEntity() {
        RestaurantEntity userEntity = createFakeUserEntity();
        Restaurant user = userEntityDataMapper.transform(userEntity);

        assertThat(user, is(instanceOf(Restaurant.class)));
        assertThat(user.getRestaurantID(), is(FAKE_USER_ID));
        assertThat(user.getAddress(), is(FAKE_FULLNAME));
    }

    @Test
    public void testTransformUserEntityCollection() {
        RestaurantEntity mockUserEntityOne = mock(RestaurantEntity.class);
        RestaurantEntity mockUserEntityTwo = mock(RestaurantEntity.class);

        List<RestaurantEntity> userEntityList = new ArrayList<RestaurantEntity>(5);
        userEntityList.add(mockUserEntityOne);
        userEntityList.add(mockUserEntityTwo);

        Collection<Restaurant> userCollection = userEntityDataMapper.transform(userEntityList);

        assertThat(userCollection.toArray()[0], is(instanceOf(Restaurant.class)));
        assertThat(userCollection.toArray()[1], is(instanceOf(Restaurant.class)));
        assertThat(userCollection.size(), is(2));
    }

    private RestaurantEntity createFakeUserEntity() {
        RestaurantEntity userEntity = new RestaurantEntity();
        userEntity.setRestaurantID(FAKE_USER_ID);
        userEntity.setName(FAKE_FULLNAME);

        return userEntity;
    }
}

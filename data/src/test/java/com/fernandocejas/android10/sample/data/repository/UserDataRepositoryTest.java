/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.repository;

import com.fernandocejas.android10.sample.data.ApplicationTestCase;
import com.fernandocejas.android10.sample.data.entity.RestaurantEntity;
import com.fernandocejas.android10.sample.data.entity.mapper.RestaurantEntityDataMapper;
import com.fernandocejas.android10.sample.data.repository.datasource.RestaurantDataStore;
import com.fernandocejas.android10.sample.data.repository.datasource.RestaurantDataStoreFactory;
import com.fernandocejas.android10.sample.domain.Restaurant;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;

public class UserDataRepositoryTest extends ApplicationTestCase {

    private static final int FAKE_USER_ID = 123;

    private RestaurantDataRepository userDataRepository;

    @Mock
    private RestaurantDataStoreFactory mockUserDataStoreFactory;
    @Mock
    private RestaurantEntityDataMapper mockUserEntityDataMapper;
    @Mock
    private RestaurantDataStore mockUserDataStore;
    @Mock
    private RestaurantEntity mockUserEntity;
    @Mock
    private Restaurant mockUser;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        userDataRepository = new RestaurantDataRepository(mockUserDataStoreFactory,
                mockUserEntityDataMapper);

        given(mockUserDataStoreFactory.create(anyInt())).willReturn(mockUserDataStore);
        given(mockUserDataStoreFactory.createCloudDataStore()).willReturn(mockUserDataStore);
    }

    @Test
    public void testGetUsersHappyCase() {
        List<RestaurantEntity> usersList = new ArrayList<>();
        usersList.add(new RestaurantEntity());
        given(mockUserDataStore.restaurantEntityList()).willReturn(Observable.just(usersList));

        userDataRepository.restaurants();

        verify(mockUserDataStoreFactory).createCloudDataStore();
        verify(mockUserDataStore).restaurantEntityList();
    }

    @Test
    public void testGetUserHappyCase() {
        RestaurantEntity userEntity = new RestaurantEntity();
        given(mockUserDataStore.restaurantEntityDetails(FAKE_USER_ID)).willReturn(Observable.just(userEntity));
        userDataRepository.restaurant(FAKE_USER_ID);

        verify(mockUserDataStoreFactory).create(FAKE_USER_ID);
        verify(mockUserDataStore).restaurantEntityDetails(FAKE_USER_ID);
    }
}

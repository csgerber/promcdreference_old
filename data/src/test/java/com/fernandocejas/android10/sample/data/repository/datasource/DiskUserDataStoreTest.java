/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.data.repository.datasource;

import com.fernandocejas.android10.sample.data.ApplicationTestCase;
import com.fernandocejas.android10.sample.data.cache.RestaurantCache;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class DiskUserDataStoreTest extends ApplicationTestCase {

    private static final int FAKE_USER_ID = 11;

    private DiskRestaurantDataStore diskUserDataStore;

    @Mock
    private RestaurantCache mockUserCache;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        diskUserDataStore = new DiskRestaurantDataStore(mockUserCache);
    }

    @Test
    public void testGetUserEntityListUnsupported() {
        expectedException.expect(UnsupportedOperationException.class);
        diskUserDataStore.restaurantEntityList();
    }

    @Test
    public void testGetUserEntityDetailesFromCache() {
        diskUserDataStore.restaurantEntityDetails(FAKE_USER_ID);
        verify(mockUserCache).get(FAKE_USER_ID);
    }
}

/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.domain.repository;

import com.fernandocejas.android10.sample.domain.Restaurant;

import java.util.List;

import rx.Observable;

/**
 * Interface that represents a Repository for getting {@link Restaurant} related data.
 */
public interface RestaurantRepository {
    /**
     * Get an {@link rx.Observable} which will emit a List of {@link Restaurant}.
     */
    Observable<List<Restaurant>> restaurants();

    /**
     * Get an {@link rx.Observable} which will emit a {@link Restaurant}.
     *
     * @param restaurantID The restaurant id used to retrieve restaurant data.
     */
    Observable<Restaurant> restaurant(final int restaurantID);
}

/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.test.mapper;

import com.fernandocejas.android10.sample.presentation.mapper.RestaurantModelDataMapper;
import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

// TODO: ignore tests for now
public class RestaurantModelDataMapperTest extends TestCase {

    private static final int FAKE_USER_ID = 123;
    private static final String FAKE_FULLNAME = "Tony Stark";

    private RestaurantModelDataMapper userModelDataMapper;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        userModelDataMapper = new RestaurantModelDataMapper();
    }

    public void testTransformUser() {
        com.fernandocejas.android10.sample.domain.Restaurant user = createFakeUser();
        RestaurantModel userModel = userModelDataMapper.transform(user);

        assertThat(userModel, is(instanceOf(RestaurantModel.class)));
        assertThat(userModel.getRestaurantID(), is(FAKE_USER_ID));
        assertThat(userModel.getFullName(), is(FAKE_FULLNAME));
    }

    public void testTransformUserCollection() {
        com.fernandocejas.android10.sample.domain.Restaurant mockUserOne = mock(com.fernandocejas.android10.sample.domain.Restaurant.class);
        com.fernandocejas.android10.sample.domain.Restaurant mockUserTwo = mock(com.fernandocejas.android10.sample.domain.Restaurant.class);

        List<com.fernandocejas.android10.sample.domain.Restaurant> userList = new ArrayList<com.fernandocejas.android10.sample.domain.Restaurant>(5);
        userList.add(mockUserOne);
        userList.add(mockUserTwo);

        Collection<RestaurantModel> userModelList = userModelDataMapper.transform(userList);

        assertThat(userModelList.toArray()[0], is(instanceOf(RestaurantModel.class)));
        assertThat(userModelList.toArray()[1], is(instanceOf(RestaurantModel.class)));
        assertThat(userModelList.size(), is(2));
    }

    private com.fernandocejas.android10.sample.domain.Restaurant createFakeUser() {
        com.fernandocejas.android10.sample.domain.Restaurant user = new com.fernandocejas.android10.sample.domain.Restaurant(FAKE_USER_ID);
        user.setAddress(FAKE_FULLNAME);

        return user;
    }
}

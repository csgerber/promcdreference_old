/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.test.presenter;

import android.content.Context;
import android.test.AndroidTestCase;

import com.fernandocejas.android10.sample.domain.interactor.GetRestaurantDetails;
import com.fernandocejas.android10.sample.presentation.mapper.RestaurantModelDataMapper;
import com.fernandocejas.android10.sample.presentation.presenter.RestaurantDetailsPresenter;
import com.fernandocejas.android10.sample.presentation.view.RestaurantDetailsView;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Subscriber;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

// TODO: ignore tests for now
public class RestaurantDetailsPresenterTest extends AndroidTestCase {

    private static final int FAKE_USER_ID = 123;

    private RestaurantDetailsPresenter userDetailsPresenter;

    @Mock
    private Context mockContext;
    @Mock
    private RestaurantDetailsView mockUserDetailsView;
    @Mock
    private GetRestaurantDetails mockGetUserDetails;
    @Mock
    private RestaurantModelDataMapper mockUserModelDataMapper;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        userDetailsPresenter = new RestaurantDetailsPresenter(mockGetUserDetails,
                mockUserModelDataMapper);
        userDetailsPresenter.setView(mockUserDetailsView);
    }

    public void testUserDetailsPresenterInitialize() {
        given(mockUserDetailsView.context()).willReturn(mockContext);

        userDetailsPresenter.initialize();

        verify(mockUserDetailsView).hideRetry();
        verify(mockUserDetailsView).showLoading();
        verify(mockGetUserDetails).execute(any(Subscriber.class));
    }
}

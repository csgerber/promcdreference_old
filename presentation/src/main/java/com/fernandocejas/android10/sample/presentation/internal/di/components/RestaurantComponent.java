/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.presentation.internal.di.components;

import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;
import com.fernandocejas.android10.sample.presentation.internal.di.modules.ActivityModule;
import com.fernandocejas.android10.sample.presentation.internal.di.modules.RestaurantModule;
import com.fernandocejas.android10.sample.presentation.view.fragment.RestaurantDetailsFragment;
import com.fernandocejas.android10.sample.presentation.view.fragment.RestaurantListFragment;

import dagger.Component;

// TODO: when moving to actiity-based instead of fragment-based, don't forget to change this

/**
 * A scope {@link com.fernandocejas.android10.sample.presentation.internal.di.PerActivity} component.
 * Injects restaurant specific Fragments.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, RestaurantModule.class})
public interface RestaurantComponent extends ActivityComponent {
    void inject(RestaurantListFragment restaurantListFragment);

    void inject(RestaurantDetailsFragment restaurantDetailsFragment);
}

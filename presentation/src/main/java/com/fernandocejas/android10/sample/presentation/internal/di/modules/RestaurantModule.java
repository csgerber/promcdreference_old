/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.presentation.internal.di.modules;

import com.fernandocejas.android10.sample.domain.executor.PostExecutionThread;
import com.fernandocejas.android10.sample.domain.executor.ThreadExecutor;
import com.fernandocejas.android10.sample.domain.interactor.GetRestaurantDetails;
import com.fernandocejas.android10.sample.domain.interactor.GetRestaurantList;
import com.fernandocejas.android10.sample.domain.interactor.UseCase;
import com.fernandocejas.android10.sample.domain.repository.RestaurantRepository;
import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides restaurant related collaborators.
 */
@Module
public class RestaurantModule {

    private int restaurantID = -1;

    public RestaurantModule() {
    }

    public RestaurantModule(int restaurantID) {
        this.restaurantID = restaurantID;
    }

    @Provides
    @PerActivity
    @Named("restaurantList")
    UseCase provideGetRestaurantListUseCase(
            GetRestaurantList getRestaurantList) {
        return getRestaurantList;
    }

    @Provides
    @PerActivity
    @Named("restaurantDetails")
    UseCase provideGetRestaurantDetailsUseCase(
            RestaurantRepository restaurantRepository, ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread) {
        return new GetRestaurantDetails(restaurantID, restaurantRepository, threadExecutor, postExecutionThread);
    }
}
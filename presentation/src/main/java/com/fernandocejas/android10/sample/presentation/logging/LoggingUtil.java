package com.fernandocejas.android10.sample.presentation.logging;

import android.util.Log;

/**
 * Created by agerber on 9/9/2016.
 */
public class LoggingUtil {

    private LoggingUtil() {
    }

    public static void logCallback( Object object, String msg) {
        Log.d("CALL_BACKS", object.getClass().getCanonicalName() + ":"+ msg + "()");
    }
}

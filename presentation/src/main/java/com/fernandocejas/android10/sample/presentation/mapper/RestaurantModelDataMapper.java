/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.presentation.mapper;

import com.fernandocejas.android10.sample.domain.Restaurant;
import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;
import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

// TODO: obviously mapping will have to be changed when switching over to restaurants

/**
 * Mapper class used to transform {@link com.fernandocejas.android10.sample.domain.Restaurant} (in the domain layer) to {@link Restaurant} in the
 * presentation layer.
 */
@PerActivity
public class RestaurantModelDataMapper {

    @Inject
    public RestaurantModelDataMapper() {
    }

    /**
     * Transform a {@link com.fernandocejas.android10.sample.domain.Restaurant} into an {@link Restaurant}.
     *
     * @param restaurant Object to be transformed.
     * @return {@link Restaurant}.
     */
    public RestaurantModel transform(Restaurant restaurant) {
        if (restaurant == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        RestaurantModel restaurantModel = new RestaurantModel(restaurant.getRestaurantID());
        restaurantModel.setImageURL(restaurant.getImageURL());
        restaurantModel.setName(restaurant.getName());
        restaurantModel.setAddress(restaurant.getAddress());
        return restaurantModel;
    }

    /**
     * Transform a Collection of {@link com.fernandocejas.android10.sample.domain.Restaurant} into a Collection of {@link Restaurant}.
     *
     * @param restaurantCollection Objects to be transformed.
     * @return List of {@link Restaurant}.
     */
    public Collection<RestaurantModel> transform(Collection<Restaurant> restaurantCollection) {
        Collection<RestaurantModel> restaurantModelsCollection;

        if (restaurantCollection != null && !restaurantCollection.isEmpty()) {
            restaurantModelsCollection = new ArrayList<>();
            for (Restaurant restaurant : restaurantCollection) {
                restaurantModelsCollection.add(transform(restaurant));
            }
        } else {
            restaurantModelsCollection = Collections.emptyList();
        }

        return restaurantModelsCollection;
    }
}

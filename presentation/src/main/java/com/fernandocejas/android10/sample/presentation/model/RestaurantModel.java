/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.presentation.model;

/**
 * Class that represents a restaurant in the presentation layer.
 */
public class RestaurantModel {

    private final int restaurantID;

    public RestaurantModel(int restaurantID) {
        this.restaurantID = restaurantID;
    }

    private String imageURL;
    private String address;
    private String name;

    public int getRestaurantID() {
        return restaurantID;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("***** RestaurantModel Details *****\n");
        stringBuilder.append("id=" + this.getRestaurantID() + "\n");
        stringBuilder.append("cover url=" + this.getImageURL() + "\n");
        stringBuilder.append("fullname=" + this.getAddress() + "\n");
        stringBuilder.append("name=" + this.getName() + "\n");
        stringBuilder.append("*******************************");

        return stringBuilder.toString();
    }
}

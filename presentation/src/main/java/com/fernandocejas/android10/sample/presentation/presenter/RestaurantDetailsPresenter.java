/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.presentation.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.fernandocejas.android10.sample.domain.Restaurant;
import com.fernandocejas.android10.sample.domain.exception.DefaultErrorBundle;
import com.fernandocejas.android10.sample.domain.exception.ErrorBundle;
import com.fernandocejas.android10.sample.domain.interactor.DefaultSubscriber;
import com.fernandocejas.android10.sample.domain.interactor.UseCase;
import com.fernandocejas.android10.sample.presentation.exception.ErrorMessageFactory;
import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;
import com.fernandocejas.android10.sample.presentation.logging.LoggingUtil;
import com.fernandocejas.android10.sample.presentation.mapper.RestaurantModelDataMapper;
import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;
import com.fernandocejas.android10.sample.presentation.view.RestaurantDetailsView;
import com.fernandocejas.frodo.annotation.RxLogSubscriber;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
@PerActivity
public class RestaurantDetailsPresenter implements Presenter {

    private RestaurantDetailsView viewDetailsView;

    private final UseCase getRestaurantDetailsUseCase;
    private final RestaurantModelDataMapper restaurantModelDataMapper;

    @Inject
    public RestaurantDetailsPresenter(@Named("restaurantDetails") UseCase getRestaurantDetailsUseCase,
                                      RestaurantModelDataMapper restaurantModelDataMapper) {
        this.getRestaurantDetailsUseCase = getRestaurantDetailsUseCase;
        this.restaurantModelDataMapper = restaurantModelDataMapper;
    }

    public void setView(@NonNull RestaurantDetailsView view) {
        this.viewDetailsView = view;
    }

    @Override
    public void resume() {
        LoggingUtil.logCallback(this, "resume");


      //  animationDrawable.start();
    }

    @Override
    public void pause() {
        LoggingUtil.logCallback(this, "pause");
    }



    @Override
    public void destroy() {
        this.getRestaurantDetailsUseCase.unsubscribe();
        this.viewDetailsView = null;
    }

    /**
     * Initializes the presenter by start retrieving restaurant details.
     */
    public void initialize() {
        this.loadRestaurantDetails();
    }

    /**
     * Loads restaurant details.
     */
    private void loadRestaurantDetails() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getRestaurantDetails();
        this.hideViewLoading();
    }

    private void showViewLoading() {
        this.viewDetailsView.showLoading();
    }

    private void hideViewLoading() {
        this.viewDetailsView.hideLoading();
    }

    private void showViewRetry() {
        this.viewDetailsView.showRetry();
    }

    private void hideViewRetry() {
        this.viewDetailsView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewDetailsView.context(),
                errorBundle.getException());
        this.viewDetailsView.showError(errorMessage);
    }

    private void showRestaurantDetailsInView(Restaurant restaurant) {
        final RestaurantModel restaurantModel = this.restaurantModelDataMapper.transform(restaurant);
        this.viewDetailsView.renderRestaurant(restaurantModel);
    }

    private void getRestaurantDetails() {
        this.getRestaurantDetailsUseCase.execute(new RestaurantDetailsSubscriber());
    }

    @RxLogSubscriber
    private final class RestaurantDetailsSubscriber extends DefaultSubscriber<Restaurant> {

        @Override
        public void onCompleted() {

          //  viewDetailsView.completedLoadingImage();
            RestaurantDetailsPresenter.this.hideViewLoading();
            Log.d("UUTT", "completed");
        }

        @Override
        public void onError(Throwable e) {
            RestaurantDetailsPresenter.this.hideViewLoading();
            RestaurantDetailsPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            RestaurantDetailsPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(Restaurant restaurant) {
            RestaurantDetailsPresenter.this.showRestaurantDetailsInView(restaurant);
            Log.d("UUTT", "next");
        }
    }


}

/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.fernandocejas.android10.sample.presentation.presenter;

import android.support.annotation.NonNull;

import com.fernandocejas.android10.sample.domain.Restaurant;
import com.fernandocejas.android10.sample.domain.exception.DefaultErrorBundle;
import com.fernandocejas.android10.sample.domain.exception.ErrorBundle;
import com.fernandocejas.android10.sample.domain.interactor.DefaultSubscriber;
import com.fernandocejas.android10.sample.domain.interactor.UseCase;
import com.fernandocejas.android10.sample.presentation.exception.ErrorMessageFactory;
import com.fernandocejas.android10.sample.presentation.internal.di.PerActivity;
import com.fernandocejas.android10.sample.presentation.mapper.RestaurantModelDataMapper;
import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;
import com.fernandocejas.android10.sample.presentation.view.RestaurantListView;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
@PerActivity
public class RestaurantListPresenter implements Presenter {

    private RestaurantListView restaurantListView;

    private final UseCase getRestaurantListUseCase;
    private final RestaurantModelDataMapper restaurantModelDataMapper;

    @Inject
    public RestaurantListPresenter(@Named("restaurantList") UseCase getRestaurantListUseCase,
                                   RestaurantModelDataMapper restaurantModelDataMapper) {
        this.getRestaurantListUseCase = getRestaurantListUseCase;
        this.restaurantModelDataMapper = restaurantModelDataMapper;
    }

    public void setView(@NonNull RestaurantListView view) {
        this.restaurantListView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {
      //  LoggingUtil.logCallback(this, "pause");
    }

    @Override
    public void destroy() {
        this.getRestaurantListUseCase.unsubscribe();
        this.restaurantListView = null;
    }

    /**
     * Initializes the presenter by start retrieving the restaurant list.
     */
    public void initialize() {
        this.loadRestaurantList();
    }

    /**
     * Loads all restaurants.
     */
    private void loadRestaurantList() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getRestaurantList();
    }

    public void onRestaurantClicked(RestaurantModel restaurantModel) {
        this.restaurantListView.viewRestaurant(restaurantModel);
    }

    private void showViewLoading() {
        this.restaurantListView.showLoading();
    }

    private void hideViewLoading() {
        this.restaurantListView.hideLoading();
    }

    private void showViewRetry() {
        this.restaurantListView.showRetry();
    }

    private void hideViewRetry() {
        this.restaurantListView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.restaurantListView.context(),
                errorBundle.getException());
        this.restaurantListView.showError(errorMessage);
    }

    private void showRestaurantsCollectionInView(Collection<Restaurant> restaurantsCollection) {
        final Collection<RestaurantModel> restaurantModelsCollection =
                this.restaurantModelDataMapper.transform(restaurantsCollection);
        this.restaurantListView.renderRestaurantList(restaurantModelsCollection);
    }

    private void getRestaurantList() {
        this.getRestaurantListUseCase.execute(new RestaurantListSubscriber());
    }

    private final class RestaurantListSubscriber extends DefaultSubscriber<List<com.fernandocejas.android10.sample.domain.Restaurant>> {

        @Override
        public void onCompleted() {
            RestaurantListPresenter.this.hideViewLoading();
        }

        @Override
        public void onError(Throwable e) {
            RestaurantListPresenter.this.hideViewLoading();
            RestaurantListPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
            RestaurantListPresenter.this.showViewRetry();
        }

        @Override
        public void onNext(List<com.fernandocejas.android10.sample.domain.Restaurant> restaurants) {
            RestaurantListPresenter.this.showRestaurantsCollectionInView(restaurants);
        }
    }
}

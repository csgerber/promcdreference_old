/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.fernandocejas.android10.sample.presentation.view;

import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;

import java.util.Collection;

/**
 * Interface representing a View in a model view presenter (MVP) pattern.
 * In this case is used as a view representing a list of {@link RestaurantModel}.
 */
public interface RestaurantListView extends LoadDataView {
    /**
     * Render a restaurant list in the UI.
     *
     * @param userModelCollection The collection of {@link RestaurantModel} that will be shown.
     */
    void renderRestaurantList(Collection<RestaurantModel> userModelCollection);

    /**
     * View a {@link RestaurantModel} profile/details.
     *
     * @param userModel The restaurant that will be shown.
     */
    void viewRestaurant(RestaurantModel userModel);
}

/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.fernandocejas.android10.sample.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.internal.di.HasComponent;
import com.fernandocejas.android10.sample.presentation.internal.di.components.DaggerRestaurantComponent;
import com.fernandocejas.android10.sample.presentation.internal.di.components.RestaurantComponent;
import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;
import com.fernandocejas.android10.sample.presentation.view.fragment.RestaurantListFragment;

/**
 * Activity that shows a list of Restaurants.
 */
public class RestaurantListActivity extends BaseActivity implements HasComponent<RestaurantComponent>,
        RestaurantListFragment.RestaurantListListener {

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, RestaurantListActivity.class);
    }

    private RestaurantComponent restaurantComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_layout);

        this.initializeInjector();
        if (savedInstanceState == null) {
            addFragment(R.id.fragmentContainer, new RestaurantListFragment());
        }
    }

    private void initializeInjector() {
        this.restaurantComponent = DaggerRestaurantComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public RestaurantComponent getComponent() {
        return restaurantComponent;
    }

    @Override
    public void onRestaurantClicked(RestaurantModel restaurantModel) {
        this.navigator.navigateToRestaurantDetails(this, restaurantModel.getRestaurantID());
    }
}

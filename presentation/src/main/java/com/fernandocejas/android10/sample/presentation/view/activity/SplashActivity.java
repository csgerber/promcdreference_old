package com.fernandocejas.android10.sample.presentation.view.activity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.fernandocejas.android10.sample.presentation.R;

import butterknife.ButterKnife;

/**
 * Main application screen. This is the app entry point.
 */
public class SplashActivity extends BaseActivity {

    private int SIMUATE_LOADING_TIME = 500;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setTheme(R.style.Theme_McD_Splash);
        mContext = this;

        //TODO add animation
        // Background code to load data on splash screen
        AsyncTask.execute(new Runnable() {
            @Override
                public void run() {
                try {
                    Thread.sleep(SIMUATE_LOADING_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                navigator.navigateToRestaurantList(mContext);
            }
        });
    }


}

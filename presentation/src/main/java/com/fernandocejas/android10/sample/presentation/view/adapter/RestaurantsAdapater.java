/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.fernandocejas.android10.sample.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Adaptar that manages a collection of {@link RestaurantModel}.
 */
public class RestaurantsAdapater extends RecyclerView.Adapter<RestaurantsAdapater.RestaurantViewHolder> {

    public interface OnItemClickListener {
        void onRestaurantClicked(RestaurantModel restaurantModel);
    }

    private List<RestaurantModel> restaurantsCollection;
    private final LayoutInflater layoutInflater;

    private OnItemClickListener onItemClickListener;

    @Inject
    public RestaurantsAdapater(Context context) {
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.restaurantsCollection = Collections.emptyList();
    }

    @Override
    public int getItemCount() {
        return (this.restaurantsCollection != null) ? this.restaurantsCollection.size() : 0;
    }

    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.row_restaurant, parent, false);
        return new RestaurantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, final int position) {
        final RestaurantModel restaurantModel = this.restaurantsCollection.get(position);
        holder.textViewTitle.setText(restaurantModel.getName());
//        holder.iv_thumbnail.setImageUrl(restaurantModel.getImageURL());
        Glide.with(layoutInflater.getContext()).load(restaurantModel.getImageURL()).into(holder.iv_thumbnail);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (RestaurantsAdapater.this.onItemClickListener != null) {
                    RestaurantsAdapater.this.onItemClickListener.onRestaurantClicked(restaurantModel);
                }
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setRestaurantsCollection(Collection<RestaurantModel> restaurantsCollection) {
        this.validateRestaurantsCollection(restaurantsCollection);
        this.restaurantsCollection = (List<RestaurantModel>) restaurantsCollection;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateRestaurantsCollection(Collection<RestaurantModel> restaurantsCollection) {
        if (restaurantsCollection == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class RestaurantViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView textViewTitle;

        @Bind(R.id.iv_thumbnail)
        ImageView iv_thumbnail;

        public RestaurantViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.fernandocejas.android10.sample.presentation.view.fragment;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.internal.di.components.RestaurantComponent;
import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;
import com.fernandocejas.android10.sample.presentation.presenter.RestaurantDetailsPresenter;
import com.fernandocejas.android10.sample.presentation.view.RestaurantDetailsView;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Fragment that shows details of a certain restaurant.
 */
public class RestaurantDetailsFragment extends BaseFragment implements RestaurantDetailsView {

    @Inject
    RestaurantDetailsPresenter restaurantDetailsPresenter;

    @Bind(R.id.iv_cover)
    ImageView iv_cover;
    @Bind(R.id.tv_name)
    TextView tv_name;
    @Bind(R.id.tv_address)
    TextView tv_address;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.rl_retry)
    RelativeLayout rl_retry;
    @Bind(R.id.bt_retry)
    Button bt_retry;

    @Bind(R.id.loader_image)
    ImageView loaderImage;

    private AnimationDrawable animationDrawable;


    public RestaurantDetailsFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(RestaurantComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View fragmentView = inflater.inflate(R.layout.fragment_restaurant_details, container, false);

        // TODO: personal note: read a bit on butterknife

        ButterKnife.bind(this, fragmentView);


        //todo move dancing fries elsewhere and center it.
        animationDrawable = (AnimationDrawable) loaderImage.getBackground();
      //  loaderImage.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
        animationDrawable.start();


        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.restaurantDetailsPresenter.setView(this);
        if (savedInstanceState == null) {
            this.loadRestaurantDetails();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        this.restaurantDetailsPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.restaurantDetailsPresenter.pause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.restaurantDetailsPresenter.destroy();
    }

    @Override
    public void renderRestaurant(RestaurantModel restaurant) {
        if (restaurant != null) {
            Glide.with(getActivity()).load(restaurant.getImageURL()).into(iv_cover);
           // this.iv_cover.setImageUrl(restaurant.getImageURL());
            this.tv_name.setText(restaurant.getName());
            this.tv_address.setText(restaurant.getAddress());
        }
    }

//    @Override
//    public void completedLoadingImage() {
//        animationDrawable.stop();
//       // animationDrawable.setVisible(false, false);
//    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
        this.getActivity().setProgressBarIndeterminateVisibility(true);

        //  loaderImage.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED);
     //  animationDrawable.stop();

      //  loaderImage.setVisibility(View.INVISIBLE);



    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
        this.getActivity().setProgressBarIndeterminateVisibility(false);
      //  loaderImage.setVisibility(View.VISIBLE);
      //  animationDrawable.start();
       // animationDrawable.setVisible(false, false);
    }

    @Override
    public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }

    /**
     * Loads all restaurants.
     */
    private void loadRestaurantDetails() {
        if (this.restaurantDetailsPresenter != null) {
            this.restaurantDetailsPresenter.initialize();
        }
    }

    @OnClick(R.id.bt_retry)
    void onButtonRetryClick() {
        RestaurantDetailsFragment.this.loadRestaurantDetails();
    }
}

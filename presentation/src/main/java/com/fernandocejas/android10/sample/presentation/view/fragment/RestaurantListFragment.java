/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.fernandocejas.android10.sample.presentation.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.fernandocejas.android10.sample.presentation.R;
import com.fernandocejas.android10.sample.presentation.internal.di.components.RestaurantComponent;
import com.fernandocejas.android10.sample.presentation.model.RestaurantModel;
import com.fernandocejas.android10.sample.presentation.presenter.RestaurantListPresenter;
import com.fernandocejas.android10.sample.presentation.view.RestaurantListView;
import com.fernandocejas.android10.sample.presentation.view.adapter.RestaurantsAdapater;
import com.fernandocejas.android10.sample.presentation.view.adapter.RestaurantsLayoutManager;

import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Fragment that shows a list of Restaurants.
 */
public class RestaurantListFragment extends BaseFragment implements RestaurantListView {

    /**
     * Interface for listening restaurant list events.
     */
    public interface RestaurantListListener {
        void onRestaurantClicked(final RestaurantModel restaurantModel);
    }

    @Inject
    RestaurantListPresenter restaurantListPresenter;
    @Inject
    RestaurantsAdapater restaurantsAdapter;

    @Bind(R.id.rv_restaurants)
    RecyclerView rv_restaurants;
    @Bind(R.id.rl_progress)
    RelativeLayout rl_progress;
    @Bind(R.id.rl_retry)
    RelativeLayout rl_retry;
    @Bind(R.id.bt_retry)
    Button bt_retry;

    private RestaurantListListener restaurantListListener;

    public RestaurantListFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof RestaurantListListener) {
            this.restaurantListListener = (RestaurantListListener) activity;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(RestaurantComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View fragmentView = inflater.inflate(R.layout.fragment_restaurant_list, container, false);
        ButterKnife.bind(this, fragmentView);
        setupRecyclerView();
        return fragmentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.restaurantListPresenter.setView(this);
        if (savedInstanceState == null) {
            this.loadRestaurantList();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.restaurantListPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.restaurantListPresenter.pause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rv_restaurants.setAdapter(null);
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.restaurantListPresenter.destroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.restaurantListListener = null;
    }

    @Override
    public void showLoading() {
        this.rl_progress.setVisibility(View.VISIBLE);
        this.getActivity().setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void hideLoading() {
        this.rl_progress.setVisibility(View.GONE);
        this.getActivity().setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void showRetry() {
        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void renderRestaurantList(Collection<RestaurantModel> restaurantCollection) {
        if (restaurantCollection != null) {
            this.restaurantsAdapter.setRestaurantsCollection(restaurantCollection);
        }
    }

    @Override
    public void viewRestaurant(RestaurantModel restaurantModel) {
        if (this.restaurantListListener != null) {
            this.restaurantListListener.onRestaurantClicked(restaurantModel);
        }
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public Context context() {
        return this.getActivity().getApplicationContext();
    }

    private void setupRecyclerView() {
        this.restaurantsAdapter.setOnItemClickListener(onItemClickListener);
        this.rv_restaurants.setLayoutManager(new RestaurantsLayoutManager(context()));
        this.rv_restaurants.setAdapter(restaurantsAdapter);
    }

    /**
     * Loads all restaurants.
     */
    private void loadRestaurantList() {
        this.restaurantListPresenter.initialize();
    }

    @OnClick(R.id.bt_retry)
    void onButtonRetryClick() {
        RestaurantListFragment.this.loadRestaurantList();
    }

    private RestaurantsAdapater.OnItemClickListener onItemClickListener =
            new RestaurantsAdapater.OnItemClickListener() {
                @Override
                public void onRestaurantClicked(RestaurantModel restaurantModel) {
                    if (RestaurantListFragment.this.restaurantListPresenter != null && restaurantModel != null) {
                        RestaurantListFragment.this.restaurantListPresenter.onRestaurantClicked(restaurantModel);
                    }
                }
            };
}
